# kernel-config

本仓库用于存放制作 `openEuler RISC-V` 版本的硬件内核镜像使用的源码。

## 开发板列表

 - TH1520
 - SG2042
 - JH7100 (VisionFive 1)
 - JH7110 (VisionFive 2)
 - D1

## 参与贡献

1. Fork 本仓库
2. 基于开发板新建分支，或 fork 现有开发板的分支
3. 提交代码
4. 新建 Pull Request

## Thanks

This repo is made possible by all current & former openEuler RISC-V sig members, and by contributiors like you. Thank you.
